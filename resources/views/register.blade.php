@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/action_page.php" method="post">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br><br>
        
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname"><br><br>
        
        <label>Gender:</label><br>
            <input type="radio" name="wn">Male<br>
            <input type="radio" name="wn">Female<br>
            <input type="radio" name="wn">Others<br><br>

        <label for="nationality">Nationality:</label><br>
        <select id="natinality">
            <option value="indonesian">Indonesian</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">inggris</option>
        </select><br><br>
        
       <label>Language Spoken</label><br>
            <input type="checkbox">Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Others<br><br>

        <label>Bio</label><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br>
            <input type="submit" value="Sign Up">
    </form>
@endsection