@extends('layout.master')

@section('judul')
Halaman Home
@endsection

@section('content')
<h1>Media Online</h1>
<h3>Sosial Media Developer</h3>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<h3>Benefit Join di Media Sosial</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama Developer</li>
    <li>Sharing Knowledge</li>
    <li>Dibuat oleh calon Web Developer terbaik</li>
</ul>
<h3>Cara Bergabung di Media Online</h3>
<ol>
    <li>Mengujungi Website ini</li>
    <li>Mendaftar di <a href="/register">Form Sign Up</a> </li>
    <li>Selesai</li>
</ol> 
@endsection
    