<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('pagewelcome', compact('fname', 'lname'));
    }
}
